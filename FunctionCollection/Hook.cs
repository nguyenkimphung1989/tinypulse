﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using AventStack.ExtentReports.Reporter;
using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace TINYPulse.FunctionCollection
{
    [Binding]
    public class Hook : GlobalFunction
    {
        private readonly IObjectContainer _objectContainer;

        private IWebDriver _driver;

        private static ExtentReports report;
        private static ExtentTest featureName;
        private static ExtentTest scenarioName;

        public Hook(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        [BeforeTestRun]
        public static void InitializeReport()
        {
            var htmlReporter = new ExtentV3HtmlReporter(@"C:\Reports\FinalReport.html");
            htmlReporter.Config.Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;

            report = new ExtentReports();
            report.AttachReporter(htmlReporter);

            DeleteOldFiles();
        }

        [AfterTestRun]
        public static void PrintReport()
        {
            report.Flush();
        }

        [BeforeFeature]
        public static void CreateFeatureNode()
        {
            featureName = report.CreateTest<Feature>(FeatureContext.Current.FeatureInfo.Title);
        }

        [BeforeScenario]
        public void Initialize()
        {
            scenarioName = featureName.CreateNode<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);

            string videoname = ScenarioContext.Current.ScenarioInfo.Title;

            _driver = new ChromeDriver(@"C:\Driver");
            _driver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromSeconds(120));
            _objectContainer.RegisterInstanceAs<IWebDriver>(_driver);
            _driver.Manage().Window.Maximize();
        }

        [AfterScenario]
        public void CleanUp()
        {
            _driver.Quit();
        }

        [BeforeStep]
        public void WaitForPageLoad()
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        [AfterStep]
        public void CreateStepNode()
        {
            var steptype = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();

            if (ScenarioContext.Current.TestError == null)
            {
                switch (steptype)
                {
                    case "Given":
                        scenarioName.CreateNode<Given>(steptype + " " + ScenarioStepContext.Current.StepInfo.Text);
                        break;
                    case "When":
                        scenarioName.CreateNode<When>(steptype + " " + ScenarioStepContext.Current.StepInfo.Text);
                        break;
                    case "Then":
                        scenarioName.CreateNode<Then>(steptype + " " + ScenarioStepContext.Current.StepInfo.Text);
                        break;
                }
            }
            else
            {
                switch (steptype)
                {
                    case "Given":
                        scenarioName.CreateNode<Given>(steptype + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                        break;
                    case "When":
                        scenarioName.CreateNode<When>(steptype + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                        break;
                    case "Then":
                        scenarioName.CreateNode<Then>(steptype + " " + ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.Message);
                        break;
                }
                var imgname = "FAILED - " + steptype + ScenarioStepContext.Current.StepInfo.Text;
                CaptureScreenShot(_driver, imgname);
                scenarioName.AddScreenCaptureFromPath(@"C:\Reports\" + imgname + ".png");
            }
        }
    }
}
