﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace TINYPulse.FunctionCollection
{
    public class GlobalFunction
    {
        /*This function will be performed to check the Element is presented or not
        Prameter: driver, by
        Return: true or false
        Throw: NoSuchElementException
        Author: Phung Nguyen*/
        public bool isElementPresent(IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        /*This function will be performed to capture screenshot
        Prameter: driver, screenshotname
        Throw: NoSuchElementException
        Author: Phung Nguyen*/
        public void CaptureScreenShot(IWebDriver driver, string screenshotname)
        {
            try
            {
                ITakesScreenshot takesScreenshot = (ITakesScreenshot)driver;
                Screenshot screenshot = takesScreenshot.GetScreenshot();
                screenshot.SaveAsFile(@"C:\Reports\" + screenshotname + ".png", ScreenshotImageFormat.Png);
            }
            catch (Exception)
            {
                Console.WriteLine("Error when capturing screenshot");
            }
        }

        /*This function will be performed to wait until the delement disappears
        Prameter: driver, by, timeout
        Return: true or false
        Throw: NoSuchElementException
        Author: Phung Nguyen*/
        public bool WaitForElementDisappears(IWebDriver driver, By by, int timeout)
        {
            for (int i = 0; i < timeout; i++)
            {
                if (!isElementPresent(driver, by))
                    return true;
                else
                {
                    Thread.Sleep(1000);
                }

            }
            return false;
        }

        /*This function will be performed to wait until the delement appears
        Prameter: driver, by, timeout
        Return: true or false
        Throw: NoSuchElementException
        Author: Phung Nguyen*/
        public bool WaitForElementAppears(IWebDriver driver, By by, int timeout)
        {
            try
            {
                if (isElementPresent(driver, by))
                {
                    for (int i = 0; i < timeout; i++)
                    {
                        if (driver.FindElement(by).Displayed)
                            return true;
                        else
                            Thread.Sleep(1000);
                    }
                }
                return false;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        /*This function will be performed to click a dynamic element
        Prameter: driver, by
        Return: true or false
        Throw: NoSuchElementException
        Author: Phung Nguyen*/
        public bool ClickADynamicElement(IWebDriver driver, By by)
        {
            try
            {
                if (isElementPresent(driver, by))
                {
                    driver.FindElement(by).Click();
                    return true;
                }
                return false;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        /*This function will be performed to clean up the Reports
        Author: Phung Nguyen*/
        public static void DeleteOldFiles()
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\Reports\");

            foreach (FileInfo fi in di.GetFiles())
            {
                fi.Delete();
            }

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }

        /*This function will be performed to simulate a click on an element by javascript
        Prameter: driver, by
        Author: Phung Nguyen*/
        public void ClickByJavascript(IWebDriver driver, By by)
        {
            IWebElement element = driver.FindElement(by);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

            js.ExecuteScript("arguments[0].click();", element);
        }
    }
}
