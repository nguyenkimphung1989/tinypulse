﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using NUnit.Framework;
using TechTalk.SpecFlow.Assist;
using System.Collections;
using System.Linq;

namespace TINYPulse.StepCollection
{
    [Binding]
    public class ManagePeopleSteps : FunctionCollection.GlobalFunction
    {
        private IWebDriver driver;

        public ManagePeopleSteps(IWebDriver remotedriver)
        {
            driver = remotedriver;
        }

        [Given(@"I have entered '(.*)' page of TINY Pulse")]
        public void GivenIHaveEnteredPageOfTINYPulse(string page)
        {
            switch (page)
            {
                case "Login":
                    driver.Navigate().GoToUrl("https://staging.tinyserver.info/signin");
                    break;
                case "SignUp":
                    driver.Navigate().GoToUrl("https://staging.tinyserver.info/engage/signup");
                    break;
                default:
                    driver.Navigate().GoToUrl("https://www.google.com/");
                    break;
            }
        }
        
        [When(@"I enter '(.*)' in Email field")]
        public void WhenIEnterInEmailField(string email)
        {
            driver.FindElement(By.XPath(ElementCollection.SignIn.txtEmail)).SendKeys(email);
        }
        
        [When(@"I enter '(.*)' in Password field")]
        public void WhenIEnterInPasswordField(string password)
        {
            driver.FindElement(By.XPath(ElementCollection.SignIn.txtPassword)).SendKeys(password);
        }
        
        [When(@"I click Sign In button")]
        public void WhenIClickSignInButton()
        {
            driver.FindElement(By.XPath(ElementCollection.SignIn.btnSignIn)).Click();
        }
        
        [When(@"I click on User and Setting icon on top menu")]
        public void WhenIClickOnUserAndSettingIconOnTopMenu()
        {
            ClickByJavascript(driver, By.XPath(ElementCollection.Engage.iconPeopleSetting));
        }
        
        [When(@"I click Add People on left menu")]
        public void WhenIClickAddPeopleOnLeftMenu()
        {
            ClickByJavascript(driver, By.XPath(ElementCollection.Engage.menuAddPeople));
        }
        
        [When(@"I add new people as below")]
        public void WhenIAddNewPeopleAsBelow(Table table)
        {
            var peopleset = table.CreateSet<PeopleDetail>();

            int index = 1;

            foreach (PeopleDetail people in peopleset)
            {
                driver.FindElement(By.XPath("(//*[@field='firstName'])[" + index + "]")).SendKeys(people.FirstName);
                driver.FindElement(By.XPath("(//*[@field='lastName'])[" + index + "]")).SendKeys(people.Email);
                driver.FindElement(By.XPath("(//*[@field='email'])[" + index + "]")).SendKeys(people.Email);
                index++;
            }
        }
        
        [When(@"I click People on left menu")]
        public void WhenIClickPeopleOnLeftMenu()
        {
            ClickByJavascript(driver, By.XPath(ElementCollection.Engage.menuPeople));
        }
        
        [When(@"I delete people as below")]
        public void WhenIDeletePeopleAsBelow(Table table)
        {
            var peopleset = table.CreateSet<PeopleDetail>();

            foreach (PeopleDetail people in peopleset)
            {
                driver.FindElement(By.XPath("//*[@data-original-title='" + people.Email + "']/ancestor::tr//div[contains(@class,'call-to-action')]")).Click();
                driver.FindElement(By.XPath(ElementCollection.Engage.menuProfiles)).Click();
                driver.FindElement(By.XPath(ElementCollection.Engage.linkPermanentlyDelete)).Click();
                driver.FindElement(By.XPath(ElementCollection.Engage.btnDeleteUser)).Click();
            }
        }
        
        [Then(@"the '(.*)' page should display")]
        public void ThenThePageShouldDisplay(string page)
        {
            switch (page)
            {
                case "Engage":
                    Assert.AreEqual(true, isElementPresent(driver, By.XPath(ElementCollection.Engage.iconPeopleSetting)));
                    break;
                case "Manage People":
                    Assert.AreEqual(true, isElementPresent(driver, By.XPath(ElementCollection.Engage.btnFilter)));
                    break;
                case "Add People":
                    Assert.AreEqual(true, isElementPresent(driver, By.XPath(ElementCollection.Engage.btnAddPeople)));
                    break;
                case "Congratulation":
                    Assert.AreEqual(true, isElementPresent(driver, By.XPath(ElementCollection.Engage.messCongratulation)));
                    break;
                default:
                    Console.WriteLine("Page does not exist!");
                    break;
            }
        }

        [When(@"I click Add People button")]
        public void WhenIClickAddPeopleButton()
        {
            driver.FindElement(By.XPath(ElementCollection.Engage.btnAddPeople)).Click();
        }
    }
}
