﻿Feature: ManagePeople

@add some new people
Scenario: Add new people
	Given I have entered 'Login' page of TINY Pulse
	When I enter 'phung1@test.com' in Email field
	When I enter '12345678Qa' in Password field
	When I click Sign In button
	Then the 'Engage' page should display
	When I click on User and Setting icon on top menu
	Then the 'Manage People' page should display
	When I click Add People on left menu
	Then the 'Add People' page should display
	When I add new people as below
	| FirstName | LastName | Email                 |
	| Phung1    | Nguyen1  | phungnguyen1@test.com |
	| Phung2    | Nguyen2  | phungnguyen2@test.com |
	| Phung3    | Nguyen3  | phungnguyen3@test.com |
	| Phung4    | Nguyen4  | phungnguyen4@test.com |
	When I click Add People button
	Then the 'Congratulation' page should display
#clean up
	When I click People on left menu
	When I delete people as below
	| FirstName | LastName | Email                 |
	| Phung1    | Nguyen1  | phungnguyen1@test.com |
	| Phung2    | Nguyen2  | phungnguyen2@test.com |
	| Phung3    | Nguyen3  | phungnguyen3@test.com |
	| Phung4    | Nguyen4  | phungnguyen4@test.com |