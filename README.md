1. Install Visual Studio Community 2019.
2. Launch Visual Studio.
3. In Extensions -> Manage Extensions: Search and Install Specflow for Visual Studio 2019.
4. Copy and paste folder Driver in repo folder into C drive.
5. Open the solution.
6. In Test -> Windows -> Enable Test Explorer (Hotkey: Ctrl + E, T).
7. Build Solution.
8. Run the test from the Test Explorer.


NOTE: 
 - Report will be generated and stored in C drive (C:\Reports) after the test executes completely.
 - Run the test against Chrome version 83.0.4103.116.